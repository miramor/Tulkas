class TulkasSession {
public:
	TulkasSession();	
	~TulkasSession();
	void processInput(void);

private:
	void processTulkasBinding(TulkasBinding &bind);
	void processInputAux(void);
	inline KeyCode getKeycode(const char *key);
	inline void grabButtons(void);
	inline void grabKeys(void);
	void processKeyPress(void);
	inline void moveCurrentWindow(int xdiff, int ydiff);
	inline void resizeCurrentWindow(int xdiff, int ydiff);

	Display * display;
	Window root;
	XWindowAttributes attr;
	XButtonEvent start;
	XEvent event;
};

inline KeyCode TulkasSession::getKeycode(const char *key) {
	KeySym sym = XStringToKeysym(key);
	return XKeysymToKeycode(this->display, sym);
}

inline void TulkasSession::grabButtons(void) {
	XGrabButton(this->display, AnyButton, KEY_MASK,
			this->root, True, ButtonPressMask,
			GrabModeAsync, GrabModeAsync,
			None, None);
}

inline void TulkasSession::grabKeys(void) {
	XGrabKey(this->display, AnyKey,
			KEY_MASK, this->root, True,
			GrabModeAsync, GrabModeAsync);
}

inline void TulkasSession::moveCurrentWindow(int xdiff, int ydiff) {
	XMoveResizeWindow(this->display, this->event.xmotion.window,
					this->attr.x + xdiff,
					this->attr.y + ydiff,
					std::max(1, this->attr.width),
					std::max(1, this->attr.height));
}

inline void TulkasSession::resizeCurrentWindow(int xdiff, int ydiff) {
	XMoveResizeWindow(this->display, this->event.xmotion.window,
					this->attr.x,
					this->attr.y,
					std::max(1, this->attr.width + xdiff),
					std::max(1, this->attr.height + ydiff));
}
