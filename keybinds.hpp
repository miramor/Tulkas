#define KEY_MASK		Mod4Mask
#define KEY_TERM		"xterm -fg white -bg gray10 -fa 'Monospace-9' &"
#define KEY_MENU		"dmenu_run -i &"
#define KEY_LOCK		"slock &"

TulkasBinding BINDINGS[] = {
	{ "p",	TULKAS_LAUNCH,	KEY_MENU },
	{ "l",	TULKAS_LAUNCH,	KEY_LOCK },
	{ "Return",	TULKAS_LAUNCH,	KEY_TERM },
	{"Up",	TULKAS_RAISE_WIN,	nullptr },
	{ "Down",	TULKAS_LOWER_WIN,	nullptr },
	{ "Left",	TULKAS_CYCLE_UP,	nullptr },
	{ "Right",	TULKAS_CYCLE_DOWN,	nullptr },
};
