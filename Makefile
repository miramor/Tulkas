CXX=g++
CXXFLAGS=-std=c++11 -O2 -mtune=native -Wall -Wextra
LDFLAGS=-lX11

SOURCES=tulkas.cpp
HEADERS=tulkas.hpp binding.hpp keybinds.hpp

tulkas: $(SOURCES) $(HEADERS)
	$(CXX) $(CXXFLAGS) -o $@ $(SOURCES) $(LDFLAGS)
