extern "C" {
#include <X11/Xlib.h>
}

#include <cassert>
#include <iostream>

#include "binding.hpp"
#include "keybinds.hpp"
#include "tulkas.hpp"

TulkasSession::TulkasSession(void) {
	this->display = XOpenDisplay(0);
	this->root = DefaultRootWindow(this->display);
}

TulkasSession::~TulkasSession(void) {
	XCloseDisplay(this->display);
}

void TulkasSession::processInput(void) {
	this->grabKeys();
	this->grabButtons();
	
	while (true) {
		XNextEvent(this->display, &(this->event));
		this->processInputAux();
	}
}

void TulkasSession::processInputAux(void) {
	if (this->event.type == KeyPress)
		this->processKeyPress();
	
	// Ready the window to be moved/resized on modkey + click
	else if (this->event.type == ButtonPress &&
			this->event.xbutton.subwindow != None) {
		XGrabPointer(this->display,
				this->event.xbutton.subwindow,
				True,
				PointerMotionMask | ButtonReleaseMask,
				GrabModeAsync, GrabModeAsync, None, None,
				CurrentTime);
		XGetWindowAttributes(this->display,
				this->event.xbutton.subwindow,
				&(this->attr));
		this->start = this->event.xbutton;

		switch(this->start.button) {
		case 1: // Raise and focus windows on move
		case 3: // Raise and focus windows on resize
			XRaiseWindow(this->display, this->event.xbutton.subwindow);
			XSetInputFocus(this->display, this->event.xbutton.subwindow, RevertToNone, CurrentTime);
			break;
			
		case 2: // Lower the window
			XLowerWindow(this->display, this->event.xbutton.subwindow);
			break;

		default:
			break;
		}

	}
	
	// Move or resize the window
	// TODO: support for move/resize without dragging, aka click-lock
	else if (this->event.type == MotionNotify) {

		int xdiff, ydiff;
		while (XCheckTypedEvent(this->display, MotionNotify,
				&(this->event))) {
			// Do nothing
		}
		
		xdiff = this->event.xbutton.x_root - this->start.x_root;
		ydiff = this->event.xbutton.y_root - this->start.y_root;
		
		switch (this->start.button) {
		case 1: // Move on modkey + left click
			moveCurrentWindow(xdiff, ydiff);
			break;

		case 3: // Resize on modkey + right click
			resizeCurrentWindow(xdiff, ydiff);
			break;
			
		default:
			break;
		}
	}
	
	// When the button is released, release the window too
	else if(this->event.type == ButtonRelease) {
		XUngrabPointer(this->display, CurrentTime);
	}
}

void TulkasSession::processKeyPress(void) {
	int kc = this->event.xkey.keycode;
	unsigned int i;

	for (i = 0; i < sizeof(BINDINGS)/sizeof(TulkasBinding); i++) {
		if (this->getKeycode(BINDINGS[i].key) == kc) {
			this->processTulkasBinding(BINDINGS[i]);
		}
	}
}

void TulkasSession::processTulkasBinding(TulkasBinding &bind) {
	switch (bind.effect)
	{
	case TULKAS_LAUNCH:
		system(bind.arguments);
		break;
	case TULKAS_LOWER_WIN:
		XLowerWindow(this->display, this->event.xkey.subwindow);
		break;
	case TULKAS_RAISE_WIN:
		XRaiseWindow(this->display, this->event.xkey.subwindow);
		XSetInputFocus(this->display, this->event.xkey.subwindow, RevertToNone, CurrentTime);
		break;
	case TULKAS_CYCLE_DOWN:
		XCirculateSubwindowsDown(this->display, this->root);
		break;
	case TULKAS_CYCLE_UP:
		XCirculateSubwindowsUp(this->display, this->root);
		break;
	default:
		break;
	}
}

int main(void) {
	TulkasSession *tsession = new TulkasSession();
	tsession->processInput();
	delete tsession;
	return 0;
}
