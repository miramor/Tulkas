#define KEY_MASK		Mod4Mask
#define KEY_TERM		"xterm -fg white -bg gray10 -fa 'Monospace-9' &"
#define KEY_MENU		"dmenu_run -i &"
#define KEY_LOCK		"slock &"
