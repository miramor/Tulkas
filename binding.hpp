typedef enum TulkasEffects {
	TULKAS_LAUNCH,
	TULKAS_RAISE_WIN,
	TULKAS_LOWER_WIN,
	TULKAS_CYCLE_UP,
	TULKAS_CYCLE_DOWN,
} TulkasEffects;

typedef struct TulkasBinding {
		const char *key;
		TulkasEffects effect;
		const char *arguments;
} TulkasBinding;
